<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nama = $_POST["nama"];
    $pekerjaan = $_POST["pekerjaan"];
    $jenis = $_POST["jenis"];
    $ban = $_POST["ban"];
    $tanggal = $_POST["tanggal"];

    // Menghitung total harga
    $harga = 0;
    switch ($jenis) {
        case "motor":
            $harga = 3000 * $ban;
            break;
        case "mobil":
            $harga = 5000 * $ban;
            break;
    }

    echo "Nama: " . $nama . "<br>";
    echo "Pekerjaan: " . $pekerjaan . "<br>";
    echo "Jenis Kendaraan: " . $jenis . "<br>";
    echo "Banyak Ban: " . $ban . "<br>";
    echo "Tanggal: " . $tanggal . "<br>";
    echo "Total Harga: Rp " . $harga . "<br>";

$file = fopen("struk.txt", "a"); // Mode "a" untuk menambahkan data ke file
    if ($file) {
        $struk = "Nama: " . $nama . "\n";
        $struk .= "Pekerjaan: " . $pekerjaan . "\n";
        $struk .= "Jenis Kendaraan: " . $jenis . "\n";
        $struk .= "Banyak Ban: " . $ban . "\n";
        $struk .= "Tanggal: " . $tanggal . "\n";
        $struk .= "Total Harga: Rp " . $harga . "\n";
        $struk .= "===============================\n\n";
        fwrite($file, $struk);
        fclose($file);
        echo "Data telah ditambahkan ke file.";
    } else {
        echo "Gagal membuka file.";
    }
}
?>