<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nama = $_POST["nama"];
    $pekerjaan = $_POST["pekerjaan"];
    $jenis = $_POST["jenis"];
    $liter = $_POST["liter"];
    $tanggal = $_POST["tanggal"];

    // Menghitung total harga
    $harga = 0;
    switch ($jenis) {
        case "pertalite":
            $harga = 10000 * $liter;
            break;
        case "pertamax":
            $harga = 12000 * $liter;
            break;
        case "dexlite":
            $harga = 12500 * $liter;
            break;
        case "solar":
            $harga = 12500 * $liter;
            break;
    }

    // Menampilkan data yang diterima
    echo "Nama: " . $nama . "<br>";
    echo "Pekerjaan: " . $pekerjaan . "<br>";
    echo "Jenis Bensin: " . $jenis . "<br>";
    echo "Liter Bensin: " . $liter . "<br>";
    echo "Tanggal: " . $tanggal . "<br>";
    echo "Total Harga: Rp " . $harga . "<br>";

    // Menulis data ke file teks
    $file = fopen("struk.txt", "a"); // Mode "a" untuk menambahkan data ke file
    if ($file) {
        $struk = "Nama: " . $nama . "\n";
        $struk .= "Pekerjaan: " . $pekerjaan . "\n";
        $struk .= "Jenis Bensin: " . $jenis . "\n";
        $struk .= "Liter Bensin: " . $liter . "\n";
        $struk .= "Tanggal: " . $tanggal . "\n";
        $struk .= "Total Harga: Rp " . $harga . "\n";
        $struk .= "===============================\n\n";
        fwrite($file, $struk);
        fclose($file);
        echo "Data telah ditambahkan ke file.";
    } else {
        echo "Gagal membuka file.";
    }
}
?>
