<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas 10</title>
</head>
<body>
    <form>
        <div>
            <label>Nilai : </label>
            <input name="nilai" type="text" placeholder="Masukkan nilai anda">
            <br><br>
        </div>

        <div>
            <button>Submit</button>
            <br><br>
        </div>
    </form>
    
<?php

$nilai = @$_GET['nilai'];

if($nilai >= 80 && $nilai <= 100){
    echo "Nilai Anda A, Baik Sekali";
}elseif($nilai >= 76.25 && $nilai <= 79.99){
    echo "Nilai Anda A-, Baik Sekali";
}elseif($nilai >= 68.75 && $nilai <= 76.24){
    echo "Nilai Anda B+, Baik";
}elseif($nilai >= 65.00 && $nilai <=  68.74){
    echo "Nilai Anda B, Baik";
}elseif($nilai >= 62.50 && $nilai <= 64.99){
    echo "Nilai Anda B-, Baik";
}elseif($nilai >= 57.50 && $nilai <=  62.49){
    echo "Nilai Anda C+, Cukup";
}elseif($nilai >= 55.00 && $nilai <=  57.49){
    echo "Nilai Anda C, Cukup";
}elseif($nilai >= 51.25  && $nilai <= 54.99){
    echo "Nilai Anda C-, Cukup";
}elseif($nilai >= 43.75 && $nilai <=  51.24){
    echo "Nilai Anda D+, Kurang";
}elseif($nilai >=40.00 && $nilai <=  43.74){
    echo "Nilai Anda D, Kurang";
}elseif($nilai >= 0.00  && $nilai <= 39.99){
    echo "Nilai Anda E, Gagal";
}else{
    echo "Nilai yang anda masukkan salah";
}
?>
</body>
</html>